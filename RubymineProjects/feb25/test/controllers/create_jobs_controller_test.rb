require 'test_helper'

class CreateJobsControllerTest < ActionController::TestCase
  setup do
    @create_job = create_jobs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:create_jobs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create create_job" do
    assert_difference('CreateJob.count') do
      post :create, create_job: {  }
    end

    assert_redirected_to create_job_path(assigns(:create_job))
  end

  test "should show create_job" do
    get :show, id: @create_job
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @create_job
    assert_response :success
  end

  test "should update create_job" do
    patch :update, id: @create_job, create_job: {  }
    assert_redirected_to create_job_path(assigns(:create_job))
  end

  test "should destroy create_job" do
    assert_difference('CreateJob.count', -1) do
      delete :destroy, id: @create_job
    end

    assert_redirected_to create_jobs_path
  end
end
